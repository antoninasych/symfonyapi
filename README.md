***For starting project: 
=============================================
*   composer install

*   set rename .env.test into .env file

*   provide connection to DB: ```DATABASE_URL=mysql://db_user:db_password@db_host:3306/db_database```

*   execute migration 
 ```php bin/console doctrine:migrations:execute --up 'DoctrineMigrations\Version20201116205509'``` 


***Create a new Order 
=============================================

**URL**: HOST/api/v1/order

**Method**: POST

***Response:***

```json
{
    "id": 21,
    "total_price": 0,
    "products": []
}
```
------------------------------------
***Add a Product to an Order 
=============================================

**URL: HOST/api/v1/order/product/add**

**Method**:POST

**Request**:

```json
{
    "id": 2,
    "products": [1,3]
}
```
**Response**:
```json
{
    "id": 2,
    "total_price": 20,
    "products": [
        {
            "id": 1,
            "name": "Cool eeee",
            "price": 10
        },
        {
            "id": 3,
            "name": "Capuchino",
            "price": 10
        }
    ]
}
```
----------------------------------
***Create a new Product 
=============================================
***UR***L: HOST/api/v1/product

***Method***:POST

***Request:***
```json
{
    "code": "latte",
    "name": "Latte",
    "description": "Nice produd",
    "price": 3
}
```

***Response:***
```json
{
    "id": 4,
    "name": "Latte",
    "price": 3
}
```

_____________________________________
***Gets an Order by ID 
=============================================
**URL:** HOST/api/v1/order/2

**METHOD:** GET

**Request:**

```json
{
    "products": []
}
```
**Response:**
```json
{
    "id": 2,
    "total_price": 20,
    "products": [
        {
            "id": 1,
            "name": "Cool eeee",
            "price": 10
        },
        {
            "id": 3,
            "name": "Capuchino",
            "price": 10
        }
    ]
}
```
