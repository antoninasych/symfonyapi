<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Customer;
use App\Entity\Order;
use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;
use phpDocumentor\Reflection\Types\Boolean;


class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function isNameExists( $name): Boolean
    {
        $queryBuilder = $this->createQueryBuilder('o');


        $name1= $queryBuilder
                ->select('COUNT(o.id)')
                ->setParameter('name', $name)
                ->getQuery()
               ;
            return ($this->count($name1)>0)?true:false;
    }


}
