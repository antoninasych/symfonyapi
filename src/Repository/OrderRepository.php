<?php

declare(strict_types=1);

namespace App\Repository;


use App\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

class OrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    public function getTotalCountByCustomer(Order $order): int
    {
        $queryBuilder = $this->createQueryBuilder('o');

        try {
            return $queryBuilder
                ->select('summ(product.price)')
                ->where($queryBuilder->expr()->eq('o.id', ':order_id'))
                ->setParameter('id', $order->getId())
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NoResultException|NonUniqueResultException $e) {
            return 0;
        }
    }

    public function getTotalPriceByCustomer(Customer $customer): int
    {
        var_dump('here');
        exit;
        return 139379;
    }
}