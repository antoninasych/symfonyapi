<?php

declare(strict_types=1);

namespace App\Dto\Response\Transformer;

use App\Dto\Exception\UnexpectedTypeException;
use App\Dto\Response\OrderResponseDto;
use App\Entity\Order;


class OrderResponseDtoTransformer extends AbstractResponseDtoTransformer
{
    private $productResponseDtoTransformer;

    public function __construct(
        ProductResponseDtoTransformer $productResponseDtoTransformer
    )
    {
        $this->productResponseDtoTransformer = $productResponseDtoTransformer;
    }

    /**
     * @param Order $order
     *
     * @return OrderResponseDto
     */
    public function transformFromObject($order): OrderResponseDto
    {
        if (!$order instanceof Order) {
            throw new UnexpectedTypeException('Expected type of Order but got ' . \get_class($order));
        }

        $dto = new OrderResponseDto();

        $dto->id = $order->getId();
        $dto->total_price = 0;
        $products=$order->getProducts()->count();
        if ($products > 0) {
             $dto->total_price = $order->getTotalPrice();
        }
        $dto->products = $this->productResponseDtoTransformer->transformFromObjects($order->getProducts());


        return $dto;
    }
}