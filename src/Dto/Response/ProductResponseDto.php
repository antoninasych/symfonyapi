<?php

declare(strict_types=1);

namespace App\Dto\Response;

use JMS\Serializer\Annotation as Serialization;

class ProductResponseDto
{
    /**
     * @Serialization\Type("integer")
     */
    public $id;
    /**
     * @Serialization\Type("string")
     */
    public $code;

    /**
     * @Serialization\Type("string")
     */
    public $name;

    /**
     * @Serialization\Type("string")
     */
    public $description;

    /**
     * @Serialization\Type("int")
     */
    public $price;
}
