<?php

declare(strict_types=1);

namespace App\Dto\Response;

use JMS\Serializer\Annotation as Serialization;

class OrderResponseDto
{
    /**
     * @Serialization\Type("integer")
     */
    public $id;
    /**
     * @Serialization\Type("integer")
     */
    public $total_price;

    /**
     * @Serialization\Type("array<App\Dto\Response\ProductResponseDto>")
     */
    public  $products;
}
