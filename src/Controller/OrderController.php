<?php

declare(strict_types=1);

namespace App\Controller;

use App\Dto\Response\Transformer\OrderResponseDtoTransformer;
use App\Entity\Order;
use App\Form\Type\OrderType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class OrderController extends AbstractApiController
{
    private  $orderResponseDtoTransformer;

    public function __construct(OrderResponseDtoTransformer $orderResponseDtoTransformer)
    {
        $this->orderResponseDtoTransformer = $orderResponseDtoTransformer;
    }

    public function showAction(Request $request): Response
    {
        $order = $this->getDoctrine()->getRepository(Order::class)->findOneBy(['id' => $request->get('id')]);

        if (!$order) {
            throw new NotFoundHttpException('Order not found');
        }
        $dto = $this->orderResponseDtoTransformer->transformFromObject($order);

        return $this->respond($dto);
    }

    public function createAction(Request $request):Response
    {
        $order = new Order();
        $order->setCreatedAt(new \DateTime());
        $this->getDoctrine()->getManager()->persist($order);
        $this->getDoctrine()->getManager()->flush();

        $dto = $this->orderResponseDtoTransformer->transformFromObject($order);

        return $this->respond($dto);
    }

    public function updateAction(Request $request): Response
    {
        $orderId = $request->get('id');

        $order = $this->getDoctrine()->getRepository(Order::class)->findOneBy(['id' => $orderId]);

        if (!$order) {
            throw new NotFoundHttpException('Order not found');
        }

        $form = $this->buildForm(OrderType::class, $order, [
            'method' => $request->getMethod(),
        ]);

        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->respond($form, Response::HTTP_BAD_REQUEST);
        }

        /** @var Order $order */
        $order = $form->getData();

        $this->getDoctrine()->getManager()->persist($order);
        $this->getDoctrine()->getManager()->flush();


        $dto = $this->orderResponseDtoTransformer->transformFromObject($order);

        return $this->respond($dto);

    }
}