<?php

declare(strict_types=1);

namespace App\Controller;

use App\Dto\Response\Transformer\OrderResponseDtoTransformer;
use App\Dto\Response\Transformer\ProductResponseDtoTransformer;
use App\Entity\Product;
use App\Form\Type\ProductType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends AbstractApiController
{
    public $productResponseDtoTransformer;

    public function indexAction(Request $request): Response
    {
        $products = $this->getDoctrine()->getRepository(Product::class)->findAll();

        return $this->respond($products);
    }

    public function __construct(ProductResponseDtoTransformer $productResponseDtoTransformer)
    {
        $this->productResponseDtoTransformer = $productResponseDtoTransformer;
    }

    public function createAction(Request $request)
    {
        $form = $this->buildForm(ProductType::class);

        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->respond($form, Response::HTTP_BAD_REQUEST);
        }

        /** @var Product $product */
        $product = $form->getData();

        $this->getDoctrine()->getManager()->persist($product);
        $this->getDoctrine()->getManager()->flush();

        $dto = $this->productResponseDtoTransformer->transformFromObject($product);

        return $this->respond($dto);


    }
}
