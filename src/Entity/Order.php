<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Integer;

/**
 * @ORM\Table(name="app_order")
 * @ORM\Entity()
 */
class Order
{


    public function __construct()
    {
        $this->products = new ArrayCollection();
    }


    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Product", cascade={"all"})
     */
    private $products;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;


    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function setId( $id): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return Collection
     */
    public function getProducts(): Collection
    {

        return (!is_null($this->products)) ? $this->products : new ArrayCollection([]);
    }

    /**
     * @param Product $product
     */
    public function addProduct(Product $product):void
    {
        if ($this->products->contains($product)) {
            return;
        }

        $this->products->add($product);
    }

    /**
     * @param Product $product
     */
    public function removeProduct(Product $product):void
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
        }
    }

    public function getTotalPrice()
    {

        $balance = 0;
        foreach ($this->products as $product) {
            $balance += $product->getPrice();
        }
        return $balance;

    }

}
